export ZSH=$HOME/.oh-my-zsh

CURRENT_DIRECTORY=~/path/to/folder
REPO_URL=path/to/gitlab

ZSH_THEME="simple"
plugins=(git laravel docker docker-compose kubectl)

# POWERLEVEL9K_DISABLE_RPROMPT=true
# POWERLEVEL9K_PROMPT_ON_NEWLINE=true
# POWERLEVEL9K_MULTILINE_LAST_PROMPT_PREFIX="▶"
# POWERLEVEL9K_MULTILINE_FIRST_PROMPT_PREFIX=""

source "$ZSH"/oh-my-zsh.sh 

alias cl='clear'
alias par='php artisan route:list'
alias pa='php artisan'
alias dc='docker-compose'
alias dcp='docker-compose ps'

# about kubecontrol
alias kn='kubens'
alias kx='kubectx'

alias gmm='git merge origin master'
alias gfs='gfo staging'
alias -g st='staging'
alias gs='git status'
alias ga='git add .'
alias gaw='gaa && git commit -m "WIP" && ggpush'

alias mm="cd $CURRENT_DIRECTORY"
# alias grafana="$CURRENT_DIRECTORY/infra/oauth2-proxy"
alias k8='mm && cd infra/k8s'
alias gl="ggpull"
alias gp="ggpush"

# alias web="mm && cd src/webclient && yarn hot&"
alias pub="mm && cd src/publisher-panel && yarn hot &"
alias land='mm && cd src/browsing-web && yarn hot &'
alias buy="mm && cd src/buy && yarn hot &"

alias dd="mm && ./deploy dev"
alias dr='mm && ./deploy recommendation'
alias srb='mm && ./start-recommendation --build'

alias pammc='php artisan module:make-controller'
alias pammm='php artisan module:make-model'
alias pammig='php artisan module:make-migration'
alias pamig='php artisan make:migration'
alias palm='php artisan module:list'
alias hot='yarn hot &'
alias pat='php artisan tinker'
alias meta='meta-git update'
alias dis='mm && cd infra/distributor'

alias j='jobs'

alias -g G='| grep'

alias -g fail="--stop-on-failure"
alias -g err='--stop-on-error'

alias -g f="--filter"
alias -g i='install'
alias -g u='update'
alias -g develop='dev'

alias ci='composer install'
alias cu='composer update'

alias t='vendor/bin/phpunit'
alias parm="php artisan migrate:rollback && php artisan migrate"
alias sai='sudo apt install'
alias say='sudo apt install -y'
alias sa='sudo apt'

alias naut='nautilus .'
alias c='code .'

alias pat='pa tinker'
alias -g apt-get="apt"
alias dcr='docker-compose -f ~/Desktop/work/mmusic/recommendation-compose.yml'
alias dcm='docker-compose -f ~/Desktop/work/mmusic/infra-compose.yml'
alias cr='composer require'

alias cdi='cd ~/ && docker rmi $(docker images -f "dangling=true" -q)'
alias cdia='cd && docker rmi $(docker images -q)'
alias kj='kill %'

alias lac='cd /home/muunii/Desktop/self/lac/syslac'
alias myip='curl http://ipecho.net/plain; echo'
alias rand='cat /dev/urandom | tr -dc A-Za-z0-9 | head -c 20 ; echo'
alias localip='ifconfig | grep 192.168'
alias drf='docker rm -f'
alias n='nano -l'
alias szsh='source ~/.zshrc'

$ gam() {
	gaa && git commit -m "$1" && ggpush || echo 'no changes'

	echo $(git_current_branch)

	if [ $(proj) = 'mmusic' ]; then
		if [ $(git_current_branch) = 'master' ]; then
				br 'gitlab.com/$REPO_URL/-/pipelines'
		elif [ $(git_current_branch) = 'staging' ];
		then
			br 'gitlab.com/$REPO_URL/-/pipelines'
		else
			br "gitlab.com/$REPO_URL/-/merge_requests/new?merge_request%5Bsource_project_id%5D=23927233&merge_request%5Bsource_branch%5D=$(git_current_branch)&merge_request%5Btarget_project_id%5D=23927233&merge_request%5Btarget_branch%5D=staging"
		fi
	fi
}

$ br() {
	URL="https://$1"
	echo "opening: {$URL}"
	xdg-open $URL > /dev/null 2>&1
}

$ dpam() {
	docker-compose exec "$1" php artisan migrate
}

$ pas() {
	php artisan serve --port="$1"
}

$ tinker() {
        dce "$1" php artisan tinker
}

$ kti() {
	keti "$1" -- php artisan tinker
}

$ gaam() {
	ga && git commit -m "$1" && ggpush
}

$ kpa() {
	keti "$1" -- php artisan "$2"
}

$ ksh() {
	keti "$1" -- sh
}

$ kpam() {
	keti "$1" -- php artisan migrate
}

$ dpa() {
	dce "$1" php artisan "$2"
}

$ proj() {
	{
		git config --local remote.origin.url|sed -n 's#.*/\([^.]*\)\.git#\1#p'
	} || {
		echo 'NO PROJECT FOUND'
	}
}

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/home/muunii/google-cloud-sdk/path.zsh.inc' ]; then . '/home/muunii/google-cloud-sdk/path.zsh.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/home/muunii/google-cloud-sdk/completion.zsh.inc' ]; then . '/home/muunii/google-cloud-sdk/completion.zsh.inc'; fi
