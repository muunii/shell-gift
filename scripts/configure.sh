# !/bin/bash

if [ -f ~/.zshrc ]; then
	rm ~/.zshrc
fi

cp .zshrc.tmp ~/.zshrc
rn .zshrc.tmp

echo "Please Run 'source ~/.zshrc'"
